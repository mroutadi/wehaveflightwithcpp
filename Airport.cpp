#include "Headers/Airport.hpp"
bool compareByTime(const Flight &a, const Flight &b) {                         //make Time ascending
  std::array<std::array<int, 2>,2> times;
  times[0][0] = std::stoi(a.getStartTime().substr(0,2));
  times[0][1] = std::stoi(a.getStartTime().substr(3,2));
  times[1][0] = std::stoi(b.getStartTime().substr(0,2));
  times[1][1] = std::stoi(b.getStartTime().substr(3,2));
  return ((times[0][0] < times[1][0]) || (times[0][0] == times[1][0] && (times[0][1] < times[1][1])));
}
bool compareByTime2(const Flight &a, const Flight &b) {                        //make Time descending
  std::array<std::array<int, 2>,2> times;
  times[0][0] = std::stoi(a.getStartTime().substr(0,2));
  times[0][1] = std::stoi(a.getStartTime().substr(3,2));
  times[1][0] = std::stoi(b.getStartTime().substr(0,2));
  times[1][1] = std::stoi(b.getStartTime().substr(3,2));
  return ((times[0][0] > times[1][0]) || (times[0][0] == times[1][0] && (times[0][1] > times[1][1])));
}
bool compareByPrice(const Flight &a, const Flight &b) {                         //make prices ascending
  return a.getCost().getPrice() < b.getCost().getPrice();
}
bool compareByPrice2(const Flight &a, const Flight &b) {                        //make prices descending
  return a.getCost().getPrice() > b.getCost().getPrice();
}
void Airport::readFile() {
  std::ifstream Read;
  try
  {
    Read.open("./Database/Airport.ap",std::ios::in);
    if (!Read.is_open())
    {
      throw std::runtime_error("Could not open file");
    }
    else
    {
      std::string x;
      while (Read.good())
      {
        getline(Read, x);
        getline(Read, x);
        us ID = std::stoi(x.substr(8, 3));
        Read >> x;          //Price word
        getline(Read, x);   //It's Price
        us pricE = std::stoi(x.substr(0, x.size()-1)); //Remove $\r
        Read >> x;          //Tax word
        getline(Read, x);   //It's Tax
        us taX = std::stoi(x.substr(0, x.size()-1)); //Remove %\r
        Read >> x;          //Pilot word
        Read >> x;          //: word
        getline(Read, x);   //It's Pilot
        us piloT = std::stoi(x.substr(0, x.size()-1)); //Remove $\r
        Read >> x;          //Dep.. word
        getline(Read, x);   //It's Dep..
        us deP = std::stoi(x.substr(0, x.size()-1)); //Remove $\r
        Cost c(pricE, taX, piloT, deP);
        Flight fli(c, ID);
        this->flights.push_back(fli);
        getline(Read, x); //Trave
        getline(Read, x); //Airplan
        getline(Read, x); //Time
        getline(Read, x); //Capacity
        Read >> x;        //Sold word
        Read >> x;        //: word
        Read >> x;        //It's SoldTicket
        us solD = std::stoi(x);
        Passenger::randomPassengers(ID, solD);
        Read >> x;        //} word
        Read >> x;        // \r
      }
    }
    Read.close();
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
}
void Airport::updateFile() {
  std::ofstream Update;
  try
  {
    Update.open("./Database/Airport.ap", std::ios::out);
    if (!Update.is_open())
    {
      throw std::runtime_error("File not opened for Save!!!");
    }
    else
    {
      int fliSize = 1;
      for (auto &item : this->flights)
      {
        Update << "{" << std::endl;
        Update << "Number: " << item.getID() << std::endl;
        Update << "Price: " << item.getCost().getPrice() << "$\r";
        Update << "Tax: " << item.getCost().getTax()<< "%\r";
        Update << "Pilot : " << item.getCost().getPilot() << "$\r";
        Update << "Depreciation: " << item.getCost().getDepreciation() << "$\r";
        Update << "Travel: " << item.getStartPoint() << " -> " << item.getFinalPoint() << "\r";
        Update << "Airplan: " << item.getAirplane() << "\r";
        Update << "Time: " << item.getStartTime() << " -> " << item.getFinalTime() << "\r";
        Update << "Capacity : " << item.getCapacity() << "\r";
        Update << "Sold : " << item.getSoldTicket() << "\r";
        Update << "}";
        if (this->flights.size() != fliSize)
        {
          Update << "\r\r";
        }
        fliSize++;
      }
    }
    Update.close();
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }

}
void Airport::operator+=(const Flight & flight) {
  this->flights.push_back(flight);
  this->updateFile();
}
void Airport::operator-=(const us flightid) {
  std::vector<Flight> newFlights;
  for (auto &item : this->flights)
  {
    if (item.getID() != flightid)
    {
      newFlights.push_back(item);
    }
  }
  this->flights = newFlights;
  this->updateFile();
}
void Airport::operator%(const std::string flName) const {
  for (auto &item : this->flights)
  {
    if (item.getStartPoint() == flName)
    {
      std::cout << item;
      break;
    }
    
  }
  
}
void Airport::operator/(const std::string flname) const {
  for (auto &item : this->flights)
  {
    if (item.getFinalPoint() == flname)
    {
      std::cout << item;
      break;
    }
    
  }
}
void Airport::operator!() const {
  us leastPrice = this->flights[0].getCost().getPrice();
  for (auto &item : this->flights)
  {
    if (item.getCost().getPrice() <= leastPrice)
    {
      leastPrice = item.getCost().getPrice();
    }
  }
  for (auto &item : this->flights)
  {
    if (item.getCost().getPrice() == leastPrice)
    {
      std::cout << item;
      std::cout << std::endl;
    } 
  }
}
void Airport::operator++() {
  std::sort(this->flights.begin() , this->flights.end(), compareByPrice);
}
void Airport::operator--() {
  std::sort(this->flights.begin() , this->flights.end(), compareByPrice2);
}
void Airport::operator++(int) {
  std::sort(this->flights.begin(), this->flights.end(), compareByTime);
}
void Airport::operator--(int) {
  std::sort(this->flights.begin(), this->flights.end(), compareByTime2);
}
void operator<<(std::ostream & out, Airport& air) {

  for (auto &item : air.flights)
  {
    out << item;
    out << std::endl;
  }
  
}