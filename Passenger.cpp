#include "Headers/Passenger.hpp"

void Passenger::randomPassengers(us fly_id, us sold_ticket) {
  std::ofstream psg;
  try
  {
    if (fly_id > 999 && fly_id < 100)
    {
      throw std::range_error("flight ID most be 3 digits!");
    }
    psg.open(("Database/"+std::to_string(fly_id) + ".ap").c_str(), std::ios::out);
    if (!psg.is_open())
    {
      throw std::runtime_error("Could not open file");
    }
    else
    {
      srand( time(NULL) );
      for (size_t i = 0; i < sold_ticket; i++)
      {
        psg << "{" << std::endl;
        // Generating Name
        std::string name = "";
        name += char((rand() % 26) + 65);
        for (size_t j = 0; j < 4; j++)
        {
          name += char((rand() % 26) + 97);
        }
        psg << "First_Name: " << name << std::endl;
        // Generating Family
        std::string lname = "";
        lname += char((rand() % 26) + 65);
        for (size_t j = 0; j < 6; j++)
        {
          lname += char((rand() % 26) + 97);
        }
        psg << "Last_Name: " << lname << std::endl;
        // Generating National ID
        std::string ID = "";
        ID += char((rand() % 9) + 49);
        for (size_t j = 0; j < 9; j++)
        {
          ID += char((rand() % 10) + 48);
        }
        psg << "National_ID: " << ID << std::endl;
        psg << "}" << std::endl;
        psg << std::endl;
      }
    }
    psg.close();
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
}
Passenger::Passenger (const std::string f, const std::string l, const std::string i):
first_name(f), last_name(l), national_id(i) {

}
std::string Passenger::getFirstName() const {
  return this->first_name;
}
std::string Passenger::getLastName() const {
  return this->last_name;
}
std::string Passenger::getNationalID() const {
  return this->national_id;
}

/*****   This isn't a member function   ******/
std::ostream & operator<<(std::ostream & out, Passenger & passenger) {
  out << std::string("Passenger: ") << passenger.first_name << 
  std::string(" ") << passenger.last_name << std::string(" ID:") <<
  passenger.national_id;
  return out;
}
/*****   This isn't a member function   ******/