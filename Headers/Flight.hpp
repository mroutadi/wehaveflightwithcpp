#ifndef FLIGHT
#define FLIGHT

#include "Cost.hpp"
#include "Passenger.hpp"
#include <fstream>
#include <iomanip>

class Flight {
friend std::ostream & operator<<(std::ostream &, Flight &);
friend std::ostream & operator<<(std::ostream &,const Flight &);
friend void operator>>(std::istream &, Flight);

private:
  Cost cost;
  const us id;
  std::vector<Passenger> passengers;
  std::string start_point;
  std::string final_point;
  std::string airplane;
  std::string start_time;
  std::string final_time;
  us capacity;
public:
  Flight (const Cost &, const us);
  Cost getCost() const;
  us getID() const;
  std::string getStartPoint() const;
  std::string getFinalPoint() const;
  std::string getAirplane() const;
  std::string getStartTime() const;
  std::string getFinalTime() const;
  us getCapacity() const;
  us getSoldTicket() const;
  bool operator>(const Flight &);
  bool operator<(const Flight &);
  void operator=(const Flight &);
  bool operator==(const Flight &);
  void operator+=(const Passenger &);
  void operator-=(const std::string);
  ui getRecivedMoney() const;
  ui getProfit() const;
};

#endif // FLIGHT
