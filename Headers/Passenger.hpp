#ifndef PASSENGER_HPP
#define PASSENGER_HPP

typedef unsigned int ui;
typedef unsigned short us;

#include <iostream>
#include <string>
#include <fstream>
#include <stdexcept>
#include <random>
#include <time.h>


class Passenger {
friend std::ostream & operator<<(std::ostream &, Passenger &);

private:
  std::string first_name;
  std::string last_name;
  std::string national_id;
public:
  static void randomPassengers(us, us);
  Passenger (const std::string, const std::string, const std::string);
  std::string getFirstName() const;
  std::string getLastName() const;
  std::string getNationalID() const;
};


#endif // PASSENGER_HPP
