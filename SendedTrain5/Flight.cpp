#include "Headers/Flight.hpp"
int flightTime(Flight);

Flight::Flight (const Cost & fCost, const us Id):id(Id),cost(fCost) {
  std::string flight;
  flight += "./Database/" + std::to_string(Id) + ".ap";
  std::ifstream fl;
  try
  {
    fl.open(flight.c_str(), std::ios::in);
    if (!fl.is_open())
    {
      throw std::runtime_error("File not opened!!!");
    }
    else
    {
      std::string nothing, name, lname, id;
      while (fl.good())
      {
        getline(fl , nothing);
        getline(fl , name);
        getline(fl , lname);
        getline(fl , id);
        fl >> nothing;
        fl >> nothing;
        Passenger psg(name.substr(12, 5), lname.substr(11, 7), id.substr(13, 10));
        this->passengers.push_back(psg);
        //std::cout << psg << std::endl;
      }
    }
    std::ifstream flightio;
    try
    {
      flightio.open("./Database/Airport.ap",std::ios::in);
      if (!flightio.is_open())
      {
        throw std::runtime_error("Could not open file");
      }
      else
      {
        std::string x;
        while (flightio.good())
        {
          getline(flightio, x);
          getline(flightio, x);
          if (x.substr(0,6) == "Number" && x.substr(8, 3) == std::to_string(Id))
          {
            getline(flightio, x); //ignore Price
            getline(flightio, x); //ignore Tax
            getline(flightio, x); //ignore Pilot
            getline(flightio, x); //ignore Dep..
            flightio >> x;
            flightio >> x;
            this->start_point = x;
            flightio >> x;
            flightio >> x;
            this->final_point = x;
            flightio >> x;
            getline(flightio, x);
            this->airplane = x.substr(1, x.size()-2);
            flightio >> x;
            flightio >> this->start_time;
            flightio >> x;
            flightio >> this->final_time;
            flightio >> x;
            flightio >> x;
            flightio >> this->capacity;
            flightio >> x;
            break;
          }
          else
          {
            for (size_t i = 0; i < 11; i++)
            {
              getline(flightio, x);
            }
          }
        }
      }
    }
    catch(const std::exception& e)
    {
      std::cerr << e.what() << '\n';
    }
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
  fl.close();
}
Cost Flight::getCost() const {
  return this->cost;
}
us Flight::getID() const {
  return this->id;
}
std::string Flight::getStartPoint() const {
  return this->start_point;
}
std::string Flight::getFinalPoint() const {
  return this->final_point;
}
std::string Flight::getAirplane() const {
  return this->airplane;
}
std::string Flight::getStartTime() const {
  return this->start_time;
}
std::string Flight::getFinalTime() const {
  return this->final_time;
}
us Flight::getCapacity() const {
  return this->capacity;
}
us Flight::getSoldTicket() const {
  return this->passengers.size();
}
void operator>>(std::istream & in, Flight fli) {
  try
  {
    std::cout << "Enter flight START time( hh:mm ): ";
    std::string tempST;
    in >> tempST;
    if (tempST[2]!=':' || (std::stoi(tempST.substr(0,2))>23 && std::stoi(tempST.substr(0,2))<0)
    || std::stoi(tempST.substr(3,2))>59 && std::stoi(tempST.substr(3,2))>60)
    {
      throw std::domain_error("Invalid Value for start time");
    }

    std::cout << "Enter flight FINAL time( hh:mm ): ";
    std::string tempFT;
    in >> tempFT;
    if (tempFT[2]!=':' || (std::stoi(tempFT.substr(0,2))>23 && std::stoi(tempFT.substr(0,2))<0)
    || std::stoi(tempFT.substr(3,2))>59 && std::stoi(tempFT.substr(3,2))>60)
    {
      throw std::domain_error("Invalid Value for final time");
    }

    std::cout << "Enter flight Capacity: ";
    us tempC;
    in >> tempC;
    if (tempC < 0)
    {
      throw std::domain_error("Invalid Value for capacity");
    }
    
    fli.start_time = tempST;
    fli.final_time = tempFT;
    fli.capacity = tempC;
    std::string tempGet;
    std::cout << "Enter flight START point: ";
    std::getline(in, tempGet);
    fli.start_point = tempGet;
    std::cout << "Enter flight FINAL point: ";
    std::getline(in, tempGet);
    fli.final_point = tempGet;
    std::cout << "Enter flight airplane name: ";
    std::getline(in, tempGet);
    fli.airplane = tempGet;
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n' << "Flight didn't change!!!" << std::endl;
  }
  
  
}
std::ostream & operator<<(std::ostream & out, Flight & fli) {
  out <<std::right << std::string("Flight ") << fli.id << " " << std::setw(12) <<fli.airplane << std::string(" From ") << std::setw(10) << fli.start_point <<
  std::string(" at ") << fli.start_time << std::string(" To ")  << std::setw(10) << fli.final_point <<
  std::string(" at ") << fli.final_time << std::string(" with ")  << std::setw(5) << fli.capacity <<
  std::string(" chair and ")  << std::setw(5) << fli.passengers.size() <<  std::string(" passenger & it's Price: ")  << std::setw(8) << fli.getCost().getPrice() << std::string("$");
}
std::ostream & operator<<(std::ostream & out, const Flight & fli) {
  out <<std::right << std::string("Flight ") << fli.id << " " << std::setw(12) <<fli.airplane << std::string(" From ") << std::setw(10) << fli.start_point <<
  std::string(" at ") << fli.start_time << std::string(" To ")  << std::setw(10) << fli.final_point <<
  std::string(" at ") << fli.final_time << std::string(" with ")  << std::setw(5) << fli.capacity <<
  std::string(" chair and ")  << std::setw(5) << fli.passengers.size() <<  std::string(" passenger & it's Price: ")  << std::setw(8) << fli.getCost().getPrice() << std::string("$");
}
bool Flight::operator>(const Flight & sec) {
  if (flightTime(*this) > flightTime(sec))
  {
    return true;
  }
  return false;
}
bool Flight::operator<(const Flight & sec) {
  //return !((*this) > sec);
  if (flightTime(*this) < flightTime(sec))
  {
    return true;
  }
  return false;
}
void Flight::operator=(const Flight & sec) {
  this->cost = sec.cost;
  this->passengers = sec.passengers;
  this->start_point = sec.start_point;
  this->start_time = sec.start_time;
  this->final_point = sec.final_point;
  this->final_time = sec.final_time;
  this->capacity = sec.capacity;
}
bool Flight::operator==(const Flight & sec) {
  return flightTime(*this) == flightTime(sec);
}
void Flight::operator+=(const Passenger & psg) {
  std::ofstream psgio;
  try
  {
    if (this->capacity >= this->passengers.size())
    {
      throw std::range_error("Airplane is full!!!");
    }
    psgio.open(("./Database/" + std::to_string(this->id) + ".ap").c_str(), std::ios::out);
    if (!psgio.is_open())
    {
      throw std::runtime_error("Could not open file so Passenger didn't add");
    }
    else
    {
      this->passengers.push_back(psg);
      for (size_t i = 0; i < this->passengers.size(); i++)
      {
        psgio << "{" << "\r";
        // writing Name
        psgio << "First_Name: " << this->passengers[i].getFirstName() << "\r";
        // writing Family
        psgio << "Last_Name: " << this->passengers[i].getLastName() << "\r";
        // writing National ID
        psgio << "National_ID: " << this->passengers[i].getNationalID() << "\r";
        psgio << "}" << "\r";
        psgio << "\r";
      }
      
    }
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
  psgio.close();
}
void Flight::operator-=(const std::string canceledPsg) {
  std::vector<Passenger> newPsgs;
  std::ofstream psgio;
  try
  {
    bool isHere = false;
    for (auto &item : this->passengers)
    {
      if (item.getNationalID() == canceledPsg)
      {
        isHere = true;
      }
    }
    if(!isHere)  throw std::runtime_error("Passenger not found!!!");
    psgio.open(("./Database/" + std::to_string(this->id) + ".ap").c_str(), std::ios::out);
    if (!psgio.is_open())
    {
      throw std::runtime_error("Could not open file so Passenger didn't remove");
    }
    else
    {
      for (size_t i = 0; i < this->passengers.size(); i++)
      {
        if (this->passengers[i].getNationalID() != canceledPsg)
        {
          newPsgs.push_back(this->passengers[i]);
        }
      }
      this->passengers = newPsgs;
      for (size_t i = 0; i < this->passengers.size(); i++)
      {
        psgio << "{\r";
        // writing Name
        psgio << "First_Name: " << this->passengers[i].getFirstName() << "\r";
        // writing Family
        psgio << "Last_Name: " << this->passengers[i].getLastName() << "\r";
        // writing National ID
        psgio << "National_ID: " << this->passengers[i].getNationalID() << "\r";
        psgio << "}\r";
        if (i != this->passengers.size()-1) psgio <<"\r";
      }
      
    }
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << '\n';
  }
  psgio.close();
}
ui Flight::getRecivedMoney() const {
  return this->passengers.size() * this->cost.getPrice();
}
ui Flight::getProfit() const {
  return (this->getRecivedMoney() - this->cost.getPilot() -
  this->cost.getDepreciation())*(100 - this->cost.getTax());
}
int flightTime(Flight flight) {
  std::string m = flight.getStartTime().substr(0,2);
  int sph = std::stoi(flight.getStartTime().substr(0,2));
  int spm = std::stoi(flight.getStartTime().substr(3,2));
  int fph = std::stoi(flight.getFinalTime().substr(0,2));
  int fpm = std::stoi(flight.getFinalTime().substr(3,2));
  int flightH, flightM;
  if (fpm < spm)
  {
    sph--;
    flightM = fpm + 60 - spm;
  }
  else flightM = fpm - spm;
  flightH = fph - sph;
  if(fph > sph) {
    flightH-=24;
  }
  int ftime = flightH*60 + flightM;
  return ftime;
}