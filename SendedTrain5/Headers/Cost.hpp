#ifndef COST_HPP
#define COST_HPP
typedef unsigned int ui;
typedef unsigned short us;

class Cost {
private:
  us price;
  us tax;
  us pilot;
  us depreciation;
public:
  Cost ();
  Cost (const us Price, const us Tax, const us Pilot, const us Depreciation);
  Cost (const Cost &);
  void setPrice(const us);
  ui getPrice() const;
  void setTax(const us);
  us getTax() const;
  void setPilot(const us);
  us getPilot() const;
  void setDepreciation(const us);
  us getDepreciation() const;
  void operator++();
  void operator--();
  void operator++(int);
  void operator--(int);
  void operator=(const Cost &);
  bool operator<(const Cost &) const;
  bool operator>(const Cost &) const;
  bool operator==(const Cost &) const;
};

#endif // COST_HPP
