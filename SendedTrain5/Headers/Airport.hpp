#ifndef AIRPORT_HPP
#define AIRPORT_HPP
#include <vector>
#include <string>
#include <array>
#include <fstream>
#include <algorithm>

#include "Flight.hpp"

class Airport {
friend void operator<<(std::ostream &, Airport&);
private:
  std::vector<Flight> flights;
public:
  void readFile();
  void updateFile();
  void operator+=(const Flight &);
  void operator-=(const us);
  void operator%(const std::string) const;
  void operator/(const std::string) const;
  void operator!() const;
  void operator++();
  void operator--();
  void operator++(int);
  void operator--(int);
};

#endif // AIRPORT_HPP
