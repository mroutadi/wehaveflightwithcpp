#include <iostream>
#include "Headers/Airport.hpp"
#include "Headers/Flight.hpp"
#include "Headers/Cost.hpp"
#include "Headers/Passenger.hpp"

using namespace std;

int main() {
  Airport a;
  a.readFile();
  cout << a;
  cout << endl;
  Cost c(1234,2, 3456, 45673);
  Flight f(c, 999);
  a += f;
  cout << a;
  cout << endl;
  a -= 102;
  cout << a;
  return 0;
}
