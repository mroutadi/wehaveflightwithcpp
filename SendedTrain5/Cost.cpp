#include "Headers/Cost.hpp"
Cost::Cost () {

}
Cost::Cost (const us Price, const us Tax, const us Pilot, const us Depreciation):
price(Price), tax(Tax), pilot(Pilot), depreciation(Depreciation) {

}
Cost::Cost (const Cost & sec) {
  this->depreciation = sec.getDepreciation();
  this->tax = sec.getTax();
  this->pilot = sec.getPilot();
  this->price = sec.getPrice();
}
void Cost::setPrice(const us pr) {
  this->price = pr;
}
ui Cost::getPrice() const {
  return this->price;
}
void Cost::setTax(const us tx) {
  this->tax = tx;
}
us Cost::getTax() const {
  return this->tax;
}
void Cost::setPilot(const us pl) {
  this->pilot = pl;
}
us Cost::getPilot() const {
  return this->pilot;
}
void Cost::setDepreciation(const us dp) {
  this->depreciation = dp;
}
us Cost::getDepreciation() const {
  return this->depreciation;
}
void Cost::operator++() {
  this->tax += 1;
}
void Cost::operator--() {
  this->tax -= 1;
}
void Cost::operator++(int) {
  this->depreciation += 1000;
}
void Cost::operator--(int) {
  this->depreciation -= 1000;
}
void Cost::operator=(const Cost & sec) {
  this->depreciation = sec.getDepreciation();
  this->tax = sec.getTax();
  this->pilot = sec.getPilot();
  this->price = sec.getPrice();
}
bool Cost::operator<(const Cost & sec) const {
  if (this->price < sec.getPrice())
  {
    return true;
  }
  return false;
}
bool Cost::operator>(const Cost & sec) const {
  return !(*this < sec);
}
bool Cost::operator==(const Cost & sec) const {
  if (this->price == sec.getPrice())
  {
    return true;
  }
  return false;
}